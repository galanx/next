<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
|			return "Welcome, $name.";
|		});
|
*/

/*Route::get('/', function()
{
	return View::make('home.index');
});
*/
/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

/*==========Next ROUTES===========*/

/*Front*/
Route::get('/', array('as'=>'next_index','uses'=>'next@index'));
Route::get('(:any)', 'next.front.pages@index');
/*Authentication*/
Route::get('admin/login', array('as'=>'auth','uses'=>'next.back.authentication@index'));
Route::post('admin/login/check', array('uses'=>'next.back.authentication@auth'));
Route::get('admin/logout',array('as'=>'logout','uses'=>'next.back.authentication@logout'));
/*Authorisation*/
Route::get('admin/register', array('as'=>'register', 'uses' => 'next.back.authorization@index'));
Route::post('admin/register/validate', array('uses'=> 'next.back.authorization@validate'));

Route::group(array('before' => 'auth'), function(){
	Route::get('admin', array('as'=>'admin','uses'=>'next.back.admin@index'));
	/*Widgets*/
	Route::get('admin/widgets', array('as' => 'widgets', 'uses' => 'next.back.widgets@index'));
	Route::get('admin/widgets/create', array('as' => 'widgets_create', 'uses' => 'next.back.widgets@create'));
	Route::post('admin/widgets/submit', array('uses' => 'next.back.widgets@submit'));

	/*Menus*/
	Route::get('admin/menus', array('as' => 'menus', 'uses' => 'next.back.menus@index'));
	Route::get('admin/menus/create', array('as' => 'menus_create', 'uses' => 'next.back.menus@create'));
	Route::get('admin/menus/view', array('as' => 'menus_view', 'uses' => 'next.back.menus@view'));
	Route::post('admin/menus/submit', array('uses' => 'next.back.menus@submit'));
	Route::delete('admin/menus/delete', array('uses' => 'next.back.menus@delete'));

	Route::get('admin/menus/(:any)/createItem', array('as' => 'menus_createItem', 'uses'=>'next.back.menus@createItem'));
	Route::get('admin/menus/(:any)/items', array('as' => 'menus_items', 'uses'=>'next.back.menus@items'));
	Route::get('admin/menus/items/(:any)/edit', array('as' => 'edit_menuItem', 'uses'=>'next.back.menus@editItem'));
	Route::post('admin/menus/submitItem', array('uses' => 'next.back.menus@submitItem'));
	Route::post('admin/menus/updateItem', array('uses' => 'next.back.menus@updateItem'));
	Route::delete('admin/menus/deleteItem', array('uses' => 'next.back.menus@deleteItem'));

	/*Pages*/
	Route::get('admin/pages', array('as'=> 'pages','uses'=>'next.back.pages@index'));
	Route::get('admin/pages/create', array('as' => 'pages_create', 'uses' => 'next.back.pages@create'));
	Route::post('admin/pages/submit', array('uses'=>'next.back.pages@submit'));
	Route::get('admin/pages/edit/(:any)', array('as' => 'edit_page', 'uses'=>'next.back.pages@edit'));

	Route::get('admin/comments', array('as'=>'comments', 'uses'=>'next.back.comment@index'));
	Route::get('admin/settings', array('as'=>'settings', 'uses'=>'next.back.settings@index'));
	Route::get('admin/users', array('as'=>'users', 'uses'=>'next.back.user@index'));
	Route::get('admin/posts', array('as'=>'posts', 'uses'=>'next.back.post@index'));
	Route::get('admin/modules', array('as'=>'modules', 'uses'=>'next.back.module@index'));
});








/*=================================*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Router::register('GET /', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to_route('auth');
});