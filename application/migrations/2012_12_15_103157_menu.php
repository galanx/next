<?php

class Menu {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menus', function($table){

			$table->increments('id');
			$table->string('name');
			$table->string('slug');
			$table->timestamps();
		});

		Schema::create('menu_items', function($table){
			$table->increments('id');
			$table->integer('menu_id');
			$table->integer('parent_id');
			$table->string('name');
			$table->string('slug');
			$table->integer('position');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menus');
		Schema::drop('menu_items');
	}

}