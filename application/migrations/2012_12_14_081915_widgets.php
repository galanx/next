<?php

class Widgets {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('widgets', function($table){

			$table->increments('id');
			$table->string('name');
			$table->string('slug');
			$table->boolean('status');
			$table->timestamps();
		});
		
		Schema::create('widget_items', function($table){

			$table->increments('id');
			$table->integer('widget_id');
			$table->integer('parent_id');
			$table->string('name');
			$table->string('slug');
			$table->text('content');
			$table->integer('position');
			$table->timestamps();	
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('widgets');
		Schema::drop('widget_items');
	}
}