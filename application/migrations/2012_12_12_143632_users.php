<?php

class Users {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table){

			$table->increments('id');
			$table->string('username');
			$table->string('password');
			$table->string('email');
			$table->integer('group_id');
			$table->string('password_reset_hash');
			$table->string('temp_password');
			$table->string('remember_me');
			$table->string('activation_hash');
			$table->string('ip_address');
			$table->string('status');
			$table->string('activated');
			$table->string('token');
			$table->string('last_login');
			
			$table->timestamps();

		});


		Schema::create('permissions', function($table){

			$table->increments('id');
			$table->integer('group_id');
			$table->boolean('can_view');
			$table->boolean('can_add');
			$table->boolean('can_edit');
			$table->boolean('can_delete');
			
		});

		Schema::create('groups', function($table){
			
			$table->increments('id');
			$table->string('name');
			$table->text('description');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('permissions');
		Schema::drop('groups');
		
	}
}