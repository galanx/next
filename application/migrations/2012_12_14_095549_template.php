<?php

class Template {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('templates', function($table){
			$table->increments('id');
			$table->string('name');
		});

		DB::table('templates')->insert(array(
			'name' => 'one-column',
		));
		DB::table('templates')->insert(array(
			'name' => 'two-columns-right',
		));
		DB::table('templates')->insert(array(
			'name' => 'two-columns-left',
		));
		DB::table('templates')->insert(array(
			'name' => 'three-columns',
		));
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('templates');
	}

}