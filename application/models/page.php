<?php
	class Page extends Eloquent{

		public static $table = "pages";

		public function templates(){

			return $this->belongs_to('Template');
		}
	}
?>