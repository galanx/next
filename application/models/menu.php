<?php
	class Menu extends Eloquent{

		public static $table = "menus";
		public static $timestamps = true;

		public function menu_items(){

			return $this->has_many('Menu_Item', 'menu_id');
		}
	}
?>