<?php
	class Template extends Eloquent{
		public static $table = 'templates';
		public static $timestamps = false;

		public function pages(){

			return $this->has_many('Page','template_id');
		}
	}
?>