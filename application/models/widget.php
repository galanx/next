<?php
	class Widget extends Eloquent{

		public static $table = 'widgets';
		public static $timestamps = true;

		public function widget_items(){

			return $this->has_many('Widget_Item', 'widget_id');
		}
	}
?>