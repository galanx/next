<?php
	
	class Widget_Item extends Eloquent{

		public static $table = 'widget_items';
		public static $timestamps = true;

		public function widgets(){

			return $this->belongs_to('Widget');
		}
	}
?>