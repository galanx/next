<?php
	
	class Menu_Item extends Eloquent{

		public static $table = 'menu_items';
		public static $timestamps = true;

		public function menus(){

			return $this->belongs_to('Menu');
		}
	}
?>