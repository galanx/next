<?php

class Next_Front_Front_Controller extends Base_Controller {

	public $layout = 'next.front.themes.next.layouts.default';
	/**
	 * Catch-all method for requests that can't be matched.
	 *
	 * @param  string    $method
	 * @param  array     $parameters
	 * @return Response
	 */

	public function __call($method, $parameters)
	{
		return Response::error('404');
	}
}