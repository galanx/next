<?php
	class Next_Front_Pages_Controller extends Next_Front_Front_Controller{

		public function action_index(){
			$curr = URI::current();

			$slugi = explode('/', $curr);

			$slug = end($slugi);

			$this->layout->content = View::make('next.front.themes.next.pages.pages')
			->with('slug', $slug);
		}
	}
?>