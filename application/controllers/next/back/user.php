<?php
	class Next_Back_User_Controller extends Next_Controller{

		public function action_index(){
			$this->layout->content = View::make('next.back.users.index')
			->with('title', 'Users');
		}
	}
?>