<?php
	class Next_Back_Pages_Controller extends Next_Controller{

		//Goes to pages backend
		public function action_index(){
			$this->layout->content = View::make('next.back.pages.index')
			->with('title','Pages');
		}

		//Creates a page
		public function action_create(){
			$templates = Template::all();
			$this->layout->content = View::make('next.back.pages.create')
			->with('templates', $templates)->with('title', 'Pages');
		}
		
		//Submits a page
		public function action_submit(){
			$insert = Page::insert(array(
				'name' => Input::get('name'),
				'slug' => Input::get('slug'),
				'content' => Input::get('content'),
				'template_id' => Input::get('template'),
			));

			if($insert){
				return Redirect::to_route('pages')
				->with('msg','Page created!');
			}else{

				return Redirect::to_route('pages')
				->with('msg', 'Failed to create a new page.');
			}
		}

		//Edits a page
		public function action_edit($id){
			$page = Page::find($id);
			$templates = Template::all();
			$this->layout->content = View::make('next.back.pages.edit')
			->with('page', $page)
			->with('templates', $templates)
			->with('title','Pages');
		}

		//Deletes a page
		public function action_delete(){
			//code here
		}
	}
?>