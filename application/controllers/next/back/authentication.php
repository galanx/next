<?php
	class Next_Back_Authentication_Controller extends Next_Controller{
		
		public $layout = "next.back.layouts.auth";
		
		//Login form view
		public function action_index(){
			$this->layout->content = View::make('next.back.authentication.index');
		}


		//Check if the credentials are correct
		public function action_auth(){
			$credentials = array(
				'username' => Input::get('username'), 
				'password' => Input::get('password'),
				);

			if(Auth::attempt($credentials)){
				return Redirect::to_route('admin');
			}else{
				return Redirect::to_route('auth')
				->with('login_errors', true);
			}
		}

		public function action_logout(){
			Auth::logout();
			return Redirect::to_route('auth');
		}
	}
?>