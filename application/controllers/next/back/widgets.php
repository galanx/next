<?php
	class Next_Back_Widgets_Controller extends Next_Controller{

		public function action_index(){

			$this->layout->content = View::make('next.back.widgets.index');
		}

		public function action_create(){

			$this->layout->content = View::make('next.back.widgets.create');
		}

		public function action_submit(){

			$insert = Widget::insert(array(
				'name' => Input::get('name'),
				'slug' => Input::get('slug'),
			));

			if($insert){
				echo "New widget added";
			}else{
				echo "Failed to add widget";
			}
		}

		public function action_view(){
			
			$this->layout->content = View::make('next.back.widgets.view');
		}

		public function action_edit(){

			$this->layout->content = View::make('next.back.widgets.edit.');
		}

		public function action_delete(){

			//code here
		}
	}
?>