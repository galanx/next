<?php
	class Next_Back_Post_Controller extends Next_Controller{

		public function action_index(){
			$this->layout->content = View::make('next.back.posts.index')
			->with('title', 'Posts');
		}
	}
?>