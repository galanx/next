<?php
	class Next_Back_Menus_Controller extends Next_Controller{

		//Goes to the menu index
		public function action_index(){

			$this->layout->content = View::make('next.back.menus.index')
			->with('title','Menus');
		}

		//Goes to the create menu form
		public function action_create(){

			$this->layout->content = View::make('next.back.menus.create')
			->with('title','Menus');
		}

		//Submits the new menu
		public function action_submit(){

			$insert = Menu::insert(array(
				'name' => Input::get('name'),
				'slug' => Input::get('slug'),
			));

			$this->layout->content = Redirect::to_route('menus_view')
			->with('title','Menus');
		}

		//Shows menu list
		public function action_view(){
			
			$this->layout->content = View::make('next.back.menus.view')
			->with('menus', Menu::all())
			->with('title','Menus');
		}

		//Shows menu items
		public function action_items($id){

			$this->layout->content = View::make('next.back.menus.items')
			->with('menu_id', Menu::find($id)->id)
			->with('title','Menus');
		}

		//Goes to edit item form
		public function action_editItem($id){

			$this->layout->content = View::make('next.back.menus.editItem')
			->with('item', Menu_Item::find($id))
			->with('title','Menus');
		}

		//Updates an item
		public function action_updateItem(){

			$item = Menu_Item::find(Input::get('id'));

			$item->name = Input::get('name');
			$item->slug = Input::get('slug');
			$item->parent_id = Input::get('parent_id');
			$item->menu_id = Input::get('menu_id');

			$item->save();

			return Redirect::to_route('menus_items',$item->menu_id)
			->with('msg', $item->name." has been updated.");
		}

		//Creates a new item
		public function action_createItem($id){

			$this->layout->content = View::make('next.back.menus.createItem')
			->with('id', Menu::find($id))
			->with('title','Menus');
		}

		//Submits the new item
		public function action_submitItem(){

			$insert = Menu_Item::insert(array(
				'name' => Input::get('name'),
				'slug' => Input::get('slug'),
				'parent_id' => Input::get('parent_id'),
				'menu_id' => Input::get('menu_id'),
			));

			return Redirect::to_route('menus_items', Menu_Item::where('slug','=',Input::get('slug'))
				->first()->menu_id);
		}

		//Edits a menu
		public function action_edit(){

			$this->layout->content = View::make('next.back.menus.edit.')
			->with('title','Menus');
		}

		//Deletes a menu
		public function action_delete()
		{
			$id = Input::get('id');
			$item = Menu_Item::where('menu_id','=',$id)->delete();

			$delete = Menu::find($id)->delete();
			return Redirect::to_route('menus_view')
			->with('msg', 'Menu deleted!');
		}

		public function action_deleteItem(){

			$id = Input::get('id');
			$menu_id = Input::get('menu_id');

			$delete = Menu_Item::find($id)->delete();

			return Redirect::to_route('menus_items',$menu_id)
			->with('msg', 'Item deleted!');
		}
	}
?>