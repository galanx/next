<?php
	class Next_Back_Module_Controller extends Next_Controller{

		public function action_index(){
			$this->layout->content = View::make('next.back.modules.index')
			->with('title', 'Modules');
		}
	}
?>