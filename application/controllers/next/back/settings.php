<?php
	class Next_Back_Settings_Controller extends Next_Controller{

		public function action_index(){
			$this->layout->content = View::make('next.back.settings.index')
			->with('title', 'Settings');
		}
	}
?>