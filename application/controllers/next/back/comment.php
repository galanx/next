<?php
	class Next_Back_Comment_Controller extends Next_Controller{

		public $restful = false;

		public function action_index(){

			$this->layout->content = View::make('next.back.comments.index')
			->with('title','Comments');
		}
	}
?>