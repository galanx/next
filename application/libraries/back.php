<?php
	class Back{
		private static $nav = array(
			"Pages",
			"Posts",
			"Comments",
			"Users",
			"Menus",
			"Widgets",
			"Themes",
			"Modules",
			"Settings",
		);

		public static $title = 'NextCMS';
		private static $icons = array(
			"<a href='/admin/comments' id='comment-link'><span id='comment'></span></a>",
		);


		//Functions
		public static function getNav(){
			echo "<ul class='nav-menu'>";
			for($i=0;$i<count(self::$nav);$i++){
				if(URI::current()=='admin/'.strtolower(self::$nav[$i])){
					echo "<li><a class='active_link' href='/admin/".strtolower(self::$nav[$i])."'>".self::$nav[$i]."</a></li>";
				}	
				else{
					echo "<li><a href='/admin/".strtolower(self::$nav[$i])."'>".self::$nav[$i]."</a></li>";
				}	
			}
			echo "</ul>";
		}

		public static function getIcons(){
			foreach(self::$icons as $icon){
				echo "<span>".$icon."</span>";
			}
		}
	}
?>