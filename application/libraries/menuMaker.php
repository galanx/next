<?php
	
	class MenuMaker{

		public static function make($id){

			self::menu($id);

		}

		public static function menu($id){

			$links = Menu_Item::where('menu_id', '=', $id)->get();

			foreach($links as $link){

				if($link->parent_id=='-1') { ?>	
					
					<ul id='links'>
						<li>
							<?php 
								echo HTML::link($link->slug, $link->name, array('id'=>$link->slug));
							?>
						</li>	
					</ul>			

					<?php //self::subMenu($link->id); 
				}	
			}
		}

		public static function subMenu($parent_id){ 

			$child = Menu_Item::where('parent_id','=',$parent_id)->get();
			if($child){
				
				echo "<ul class='childMenu'>";
						foreach($child as $child){echo "<div class='children p$child->parent_id' >";
							echo "<li>".HTML::link($child->slug, $child->name); 
								echo self::subMenu($child->id);
							echo "</li></div>";
						}
				echo "</ul>";
				}	
		 }

		 public static function link($element, $route, $id){

		 	return HTML::link_to_route($route, $element, $id);
		 }


		public static function deleteChildren($parent_id){
			$children = Menu_Item::where('parent_id', '=', $parent_id)->get();

			foreach($children as $child){
				self::deleteChildren($child->id);
			}

		 	$child = Menu_Item::where('parent_id','=',$parent_id)->delete();
		}
	}		
?>