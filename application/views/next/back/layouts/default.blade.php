@include('next.back.layouts.partials.header')
<div id="main-container-backend">
			<div id="sidebar-backend">
				<?php Back::getNav() ?>
			</div>
			<div id="content-backend">{{$content}}</div>
</div>
@include('next.back.layouts.partials.footer')	