<head>
	<title>{{Back::$title}}</title>
	{{HTML::style('../css/auth.css')}}
</head>
<body>

	<div id="logIn">{{HTML::image('img/next_logo.png', '', array('id'=>'logo'))}}
		{{$content}}
	</div>	
</body>
