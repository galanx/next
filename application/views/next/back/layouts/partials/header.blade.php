<!DOCTYPE html>
<html>
	<head>
		<title>{{Back::$title}}</title>
		<!--[if IE]>
		<style type='text/css'>
			.icons{display:none;}
			*{border:none;}
		</style>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		{{HTML::style('../css/backend.css')}}
	</head>
	<body>
		<header>
			<a href="/admin">{{HTML::image('img/next_logo.png', '', array('id'=>'logo'))}}</a>
			<div class="site"><a href="/">{{HTML::image('img/site_icon.png','',array('id'=>'site_icon'))}}<span>{{Front::$title}}</span></a></div>
			<div id="user"><?php Back::getIcons();?><p>Logged in as <span id="username">{{Auth::user()->username}}</span>. {{HTML::link_to_route('logout',"Log out")}}</p></div>
		</header>
