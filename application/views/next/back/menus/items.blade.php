<h1>{{$title}}</h1>
<div id="content">
<?php $items = Menu_Item::where('menu_id','=',$menu_id)->get()?>
@foreach($items as $item)
{{HTML::link_to_route('edit_menuItem', $item->name, $item->id)}}<br/>
@endforeach
<br/><br/>
{{HTML::link_to_route('menus_createItem', 'Add new item', $menu_id)}} | 
{{Form::open('admin/menus/delete', 'DELETE')}}
{{Form::hidden('id', $menu_id)}}
{{Form::submit('Delete Menu')}}
{{Form::close()}}<br/><br/>
</div>