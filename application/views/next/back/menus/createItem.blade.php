<h1>{{$title}}</h1>
<div id="content">
<?php $links = Menu_Item::where('menu_id','=',$id->id)->get() ?>
{{Form::open('admin/menus/submitItem', 'POST')}}
{{Form::label('name', 'Name')}}<br/>
{{Form::text('name')}}<br/>
{{Form::label('slug','Slug')}}<br/>
{{Form::text('slug')}}<br/>
<p>{{Form::label('parent_id', 'Parent')}}<br/>
	<select name="parent_id">
		<option SELECTED value="-1">None</option>
		<?php

			foreach($links as $link){
				echo "<option value=$link->id>$link->name</option>";
			}
				
		?>
	</select></p>
{{Form::hidden('menu_id', $id->id)}}
{{Form::submit('Submit')}}<br/>
{{Form::close()}}
</div>