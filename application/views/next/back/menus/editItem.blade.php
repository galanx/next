<?php $links = Menu_Item::where('menu_id','=',$item->menu_id)->get(); ?>
<?php $parent = Menu_Item::find($item->parent_id); ?>

<h1>Editing {{$item->name}}</h1>
<div id="content">
{{Form::open('admin/menus/updateItem', 'POST')}}
{{Form::label('name', 'Name')}}<br/>
{{Form::text('name', $item->name)}}<br/>
{{Form::label('slug','Slug')}}<br/>
{{Form::text('slug', $item->slug)}}<br/>
<p>{{Form::label('parent_id', 'Parent')}}<br/>
	<select name="parent_id">
	<?php	
	if($item->parent_id!='-1'){ ?>
			<option SELECTED value='{{$item->parent_id}}'>{{$parent->name}}</option>
	<?php }	

	else{ ?>
			<option SELECTED value='-1'>None</option>
	<?php }
		
			foreach($links as $link){
				if($link->name==$item->name||$link->parent_id==$item->id||
					$link->id==$item->parent_id||$item->parent_id=='-1'){
					//nothing :D
				}else{
					echo "<option value=$link->id>$link->name</option>";
				}	
			}
	?>
	</select></p>
{{Form::hidden('menu_id', $item->menu_id)}}
{{Form::hidden('id', $item->id)}}
{{Form::submit('Submit')}}
{{Form::close()}}

{{Form::open('admin/menus/deleteItem', 'DELETE')}}
{{Form::hidden('id', $item->id)}}
{{Form::hidden('menu_id', $item->menu_id)}}
{{Form::submit('Delete Item')}}
{{Form::close()}}<br/><br/>
</div>

