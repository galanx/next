@if($page)
<h1>Editing {{$page->name}}</h1>
<div id="content">
{{Form::open('admin/pages/update', 'POST')}}
{{Form::label('name', 'Name:')}}<br/>
{{Form::text('name', $page->name)}}<br/>
{{Form::label('slug', 'Slug:')}}<br/>
{{Form::text('slug', $page->slug)}}<br/>
{{Form::label('content', 'Content:')}}<br/>
{{Form::textarea('content')}}<br/>
<select name="template">
	@foreach($templates as $template)
		<option name="template" value="{{$template->id}}">{{$template->name}}</option>
	@endforeach
</select><br/>
{{Form::submit('Submit')}}<br/>
{{Form::close()}}
@else
	<p>There is no page to edit.</p>
@endif
</div>