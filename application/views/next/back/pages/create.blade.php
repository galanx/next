{{HTML::script('js/wysiwyg.js')}}
	
<body onload="iFrameOn();">
<h1>{{$title}}</h1>
<div id="content">
{{Form::open('admin/pages/submit', 'POST', array('id'=>'pages'))}}
{{Form::label('name', 'Name:')}}<br/>
{{Form::text('name')}}<br/>
{{Form::label('slug', 'Slug:')}}<br/>
{{Form::text('slug')}}<br/>
{{Form::label('content', 'Content:')}}<br/>
<div id="wysiwyg_cp">
	<input type="button" onclick="iBold()" value="B"/>
	<input type="button" onclick="iUnderline()" value="U"/>
	<input type="button" onclick="iItalic()" value="I"/>
	<input type="button" onclick="iFontSize()" value="Text Size"/>
	<input type="button" onclick="iForeColor()" value="Text Color"/>
	<input type="button" onclick="iHorizontalRule()" value="HR"/>
	<input type="button" onclick="iUnorderedList()" value="UL"/>
	<input type="button" onclick="iOrderedList()" value="OL"/>
	<input type="button" onclick="iLink()" value="Link"/>
	<input type="button" onclick="iUnlink()" value="UnLink"/>
	<input type="button" onclick="iImage()" value="Image"/>
</div>
<textarea style="display:none" name="content" id="content" cols="100" rows="14"></textarea>
<iframe name="wysiwyg" id="wysiwyg" style="border:#000 1px solid; width:700px; height:300px;"></iframe><br/>
<select name="template">
	@foreach($templates as $template)
		<option name="template" value="{{$template->id}}">{{$template->name}}</option>
	@endforeach
</select><br/>
{{Form::submit('Submit', array('type="button", onClick="javascript:submit_form();"'))}}<br/>
{{Form::close()}}
</div>
</body>
