<h1>{{$title}}</h1>
<div id="content">
<?php $pages = Page::all() ?>
@if($pages)
	@foreach($pages as $page)
		{{HTML::link_to_route('edit_page',$page->name,$page->id)}}<br/>
	@endforeach
@else
There are no pages.
@endif<br/><br/>
<div id="button-type">{{HTML::link_to_route('pages_create', 'Create a new page')}}</div>
</div>