@if(Session::has('login_errors'))
    <p class="error">Username or password incorrect.</p>
@endif
@if(Session::has('msg'))
<p class="msg">{{Session::get('msg')}}</p>
@endif
{{Form::open('admin/login/check', 'POST')}}
{{Form::label('username','Username:')}}<br/>
{{Form::text('username')}}<br/><br/>
{{Form::label('password', 'Password:')}}<br/>
{{Form::password('password')}}<br/><br/>
{{Form::checkbox('remember')}}
{{Form::label('remember', 'Remember me')}}<br/><br/>
{{HTML::link_to_route('register',"Don't have account yet?")}}</br><br/>
{{Form::token()}}
{{Form::submit('Log in', array('class'=>'button'))}}
{{Form::close()}}