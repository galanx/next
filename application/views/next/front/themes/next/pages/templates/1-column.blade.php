
		<div class = "one-col">
			<div id="content">
				
				@if($page)
					</br>
					<h1>{{$page->title}}</h1></br>			
					<p>{{$page->content}}</p>

				@else
					</br>
					<h1>The {{$slug}} page has not been created yet</h1>
					</br>
					<p id="notification">This page has not been created yet. If you wish to create a new page,
					please go {{ HTML::link('admin/pages', 'here') }}.</p>	
				@endif

			</div>
		</div>
