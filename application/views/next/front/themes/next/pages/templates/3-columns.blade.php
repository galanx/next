
		<div class = "three-col">
			<aside id="side-two">SIDEBAR</aside>
			<div id="content">
				

				@if($page)
					</br>
					<h1>{{$page->title}}</h1></br>			
					<p>{{$page->content}}</p>

				@else
					</br>
					<h1>The {{$slug}} page has not been created yet</h1>
					</br>
					<p>This page has not been created yet. If you wish to create a new page,
					please go {{ HTML::link('admin/pages', 'here') }}.</p>	
				@endif

			</div>
			<aside id="side">SIDEBAR</aside>	
		</div>
