<?php 
	$page = Page::where('slug','=',$slug)->first(); 
	
	$template = '';
	if($page){
		 $template = Template::find($page->template_id)->name;	
	}

	switch ($template) {
		case 'one-column':
			?>
			@include('next.front.themes.next.pages.templates.1-column')
			<?php
		break;
		case 'two-columns-right':
			?>
			@include('next.front.themes.next.pages.templates.2-columns-right')
			<?php
		break;
		case 'two-columns-left':
			?>
			@include('next.front.themes.next.pages.templates.2-columns-left')
			<?php
		break;
		case 'three-columns':
			?>
			@include('next.front.themes.next.pages.templates.3-columns')
			<?php
		break;
		
		default:
			?>
			@include('next.front.themes.next.pages.templates.1-column')
			<?php
			break;
	}
?>