<!DOCTYPE html>
<html>
	<head>
		<title>{{Front::$title}}</title>
		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		{{HTML::style('../css/styles.css')}}
	</head>
	<body>
			<header>
				<section>
					<div id="mark">
						<a href="/">{{HTML::image('img/next_logo.png', '', array('id'=>'logo'))}}
						</a>
					</div>	
					<div id="top-links">
						{{MenuMaker::make(1)}}
					</div>
				</section>
			</header>