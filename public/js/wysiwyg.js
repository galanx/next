function iFrameOn(){
		wysiwyg.document.designMode = "on";
	}

	function iBold(){
		wysiwyg.document.execCommand('bold',false,null);
	}

	function iUnderline(){
		wysiwyg.document.execCommand('underline',false,null);
	}

	function iItalic(){
		wysiwyg.document.execCommand('italic',false,null);
	}

	function iFontSize(){
		var size = prompt('Enter a size 1-7','');
		wysiwyg.document.execCommand('FontSize',false,size);
	}

	function iForeColor(){
		var color = prompt('Enter a color name/code','');
		wysiwyg.document.execCommand('ForeColor',false,color);
	}

	function iHorizontalRule(){
		wysiwyg.document.execCommand('InsertHorizontalRule',false,null);
	}

	function iUnorderedList(){
		wysiwyg.document.execCommand('InsertUnorderedList',false,"newUL");
	}

	function iOrderedList(){
		wysiwyg.document.execCommand('InsertOrderedList',false,"newOL");
	}

	function iLink(){
		var url = prompt('Enter url','http://');
		wysiwyg.document.execCommand('CreateLink',false,url);
	}

	function iUnlink(){
		wysiwyg.document.execCommand('Unlink',false,null);
	}

	function iImage(){
		var imgSrc = prompt('Enter image path:', '');
		if(imgSrc != null)
			wysiwyg.document.execCommand('insertimage',false,imgSrc);
	}

	function submit_form(){
		var theForm = document.getElementById('pages');
		theForm.elements["content"].value = window.frames["wysiwyg"].document.body.innerHTML;
		theForm.submit();
	}